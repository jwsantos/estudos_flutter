import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:teste/app_Controler.dart';
import 'home_Page.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: AppControler.instance,
      builder: (context, child) {
       return MaterialApp(
          theme: ThemeData(
            primarySwatch: Colors.red,
            brightness: AppControler.instance.isDartTheme ?
            Brightness.light :
            Brightness.dark,
          ),
          home: homePage(),
        );
      },
    );
  }
}