import 'package:flutter/material.dart';
import 'package:teste/app_Controler.dart';

class homePage extends StatefulWidget {
  @override
  State<homePage> createState() {
    return homePageState();
  }
}

class homePageState extends State<homePage> {
  int conter = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar (

        title: Text('PROJETO DAKI'),
          actions: [CustomSwitch(),]
      ),
      floatingActionButton: FloatingActionButton(onPressed: () {
        setState(() {
          conter++;
        });
      },
      child: Icon(Icons.add),

      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment:  CrossAxisAlignment.start,
          children: [
            Container(height: 100),
            CustomSwitch(),
            Container(height: 100),

            Text('Contador:$conter'),
            Row(
              mainAxisAlignment:  MainAxisAlignment.spaceAround,
              crossAxisAlignment:  CrossAxisAlignment.start,
              children: [
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.black,
                ),
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.black,
                ),
                Container(
                  width: 50,
                  height: 50,
                  color: Colors.black,
                ),

              ],

            ),
          ],

        ),
      ),



      );


  }
}

class CustomSwitch extends StatelessWidget {
  const CustomSwitch({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: AppControler.instance.isDartTheme,
      onChanged: (value){
        AppControler.instance.chanceTheme();

      },

    );
  }
}

